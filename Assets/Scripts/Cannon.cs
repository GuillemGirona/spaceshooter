﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{

    public float couldown;
    private float timeCounter;
    private bool canShoot;
    Vector3 pos1;
    Vector3 pos2;

    //Create bullets


    

    void Start()
    {
        canShoot = true;
        timeCounter = 0;
    }

    void Update()
    {
        if(!canShoot)
        {
            timeCounter += Time.deltaTime;
            if(timeCounter >= couldown)
            {
                canShoot = true;
            }
        }
    }

    public void ShootCannon(Bullet bullet, Vector3 position, float rotation)
    {
        if(canShoot)
        {
            canShoot = false;
            timeCounter = 0;
            bullet.Shot(position, rotation);
        }
    }


}