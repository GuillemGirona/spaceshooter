﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {
	public float speed;
	private bool shooting = false;
	private Vector3 iniPos;

	void start(){
		iniPos = transform.position;
	}

	public void Shot(Vector3 position, float direction){
		transform.position = position;
		shooting = true;
        transform.rotation = Quaternion.Euler(0, 0, direction);
	}
	
	// Update is called once per frame
	void Update () {
		if (shooting) {
			transform.Translate (0, speed * Time.deltaTime, 0);
		}
	}

	public void OnTriggerEnter2D(Collider2D other){
		if (other.tag == "Finish") {
			transform.position = iniPos;
			shooting = false;
		}
	}
}
