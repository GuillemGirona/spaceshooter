﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour {

    public Cannon cannon;
    private Cartridge cart;
    public int maxBullets;
    public Transform bulletPosition;

    public void ShootWeapon()
    {
        cannon.ShootCannon(cart.GetBullet(), transform.position, 0); 
    }
   private void Start()
    {

        GameObject[] bullet = Resources.LoadAll <GameObject> ("Prefabs/Bullet");

        cart = new Cartridge(bullet[0], maxBullets, bulletPosition);
    }
        


}
